<?php

namespace Drupal\entity_update_helper\Service;

use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityDefinitionUpdateManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\BaseFieldDefinition;

/**
 * Helps perform field / entity updates.
 */
class EntityUpdateHelper {

  /**
   * Drupal database connection service.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Entity definition update manager service.
   *
   * @var \Drupal\Core\Entity\EntityDefinitionUpdateManagerInterface
   */
  protected $entityDefinitionUpdateManager;

  /**
   * Entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * UpdateHelper constructor.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   Drupal database connection service.
   * @param \Drupal\Core\Entity\EntityDefinitionUpdateManagerInterface $entityDefinitionUpdateManager
   *   Entity definition update manager service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager service.
   */
  public function __construct(
    Connection $database,
    EntityDefinitionUpdateManagerInterface $entityDefinitionUpdateManager,
    EntityTypeManagerInterface $entityTypeManager) {
    $this->database = $database;
    $this->entityDefinitionUpdateManager = $entityDefinitionUpdateManager;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * Registers a new base field definition to database.
   *
   * @param string $entityTypeId
   *   Entity type ID, e.g.: 'node', 'your_custom_entity_id'.
   * @param \Drupal\Core\Field\BaseFieldDefinition $baseFieldDefinitionReplacement
   *   Base field definition to add.
   * @param string $newFieldName
   *   The new field name. Must match with the definition on the entity.
   */
  public function addBaseFieldDefinition(
    $entityTypeId,
    BaseFieldDefinition $baseFieldDefinitionReplacement,
    $newFieldName) {
    // Install the new definition.
    $this->entityDefinitionUpdateManager->installFieldStorageDefinition($newFieldName, $entityTypeId, $entityTypeId, $baseFieldDefinitionReplacement);
  }

  /**
   * Performs replacement of a base field for single-to-single cardinality.
   *
   * @param string $entityTypeId
   *   Entity type ID, e.g.: 'node', 'your_custom_entity_id'.
   * @param \Drupal\Core\Field\BaseFieldDefinition $baseFieldDefinitionReplacement
   *   Base field definition to add.
   * @param string $oldFieldName
   *   The old field name to replace.
   * @param string $newFieldName
   *   The new field name. Must match with the definition on the entity.
   *   Defaults to the oldFieldName value.
   * @param callable|null $dataTransformer
   *   Callable function which will be called before inserting the data into
   *   the new field. Leave empty if there is no need to transform the data.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function replaceBaseFieldDefinitionSingleToSingle(
    $entityTypeId,
    BaseFieldDefinition $baseFieldDefinitionReplacement,
    $oldFieldName,
    $newFieldName = NULL,
    callable $dataTransformer = NULL) {
    if (empty($newFieldName)) {
      // Keep the same field name.
      $newFieldName = $oldFieldName;
    }

    $storage = $this->entityTypeManager->getStorage($entityTypeId);
    $entityTypeDefinition = $this->entityTypeManager->getDefinition($entityTypeId);

    // Sometimes the primary key isn't 'id'. e.g. 'eid' or 'item_id'.
    $idKey = $entityTypeDefinition->getKey('id');

    // Get the field storage definition.
    $fieldStorageDefinition = $this->entityDefinitionUpdateManager->getFieldStorageDefinition($oldFieldName, $entityTypeId);

    // Store temporarily the existing values.
    $originalFieldTable = $storage->getBaseTable();
    $valuesToTransfer = $this->database->select($originalFieldTable)
      ->fields($originalFieldTable, [$idKey, $oldFieldName])
      ->execute()
      ->fetchAllKeyed();

    // Clear out the values from the field to replace.
    $this->database->update($originalFieldTable)
      ->fields([$oldFieldName => NULL])
      ->execute();

    // Uninstall the field.
    $this->entityDefinitionUpdateManager->uninstallFieldStorageDefinition($fieldStorageDefinition);

    // Install the new definition.
    $this->entityDefinitionUpdateManager->installFieldStorageDefinition($newFieldName, $entityTypeId, $entityTypeId, $baseFieldDefinitionReplacement);

    if (!empty($dataTransformer)) {
      // Transform the original data to fit in the new field.
      array_walk($valuesToTransfer, $dataTransformer);
    }

    // Restore the values.
    foreach ($valuesToTransfer as $id => $value) {
      $this->database->update($originalFieldTable)
        ->fields([$newFieldName => $value])
        ->condition($idKey, $id)
        ->execute();
    }
  }

  /**
   * Deletes a base field definition from database.
   *
   * Must be called after the code was removed. CRON must have run right after
   * to perform cleanup.
   *
   * @param string $entityTypeId
   *   The entity type id, e.g.: 'node'.
   * @param string $fieldNameToDelete
   *   Name of the field to remove from database.
   */
  public function deleteBaseFieldDefinition($entityTypeId, $fieldNameToDelete) {
    $definition = $this->entityDefinitionUpdateManager->getFieldStorageDefinition($fieldNameToDelete, $entityTypeId);
    $this->entityDefinitionUpdateManager->uninstallFieldStorageDefinition($definition);
  }

}
